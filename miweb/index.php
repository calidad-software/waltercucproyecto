<?php 
	session_start();
include("config/connectordb.php") ?>

<?php include("resources/views/header.php") ?>

<div class="container">
	<div class="row">
		<div class="col">
			<h1 class="text-center mb-4">Buscador de CUI</h1>
			<!-- Validacion de campo vacio -->
			<?php if (isset($_SESSION['val1'])) { ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<?= $_SESSION['val1']?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php session_unset(); } ?>

			<!-- Validacion de menor a 13 caracteres -->
			<?php if (isset($_SESSION['val2'])) { ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<?= $_SESSION['val2']?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php session_unset(); } ?>

			<!-- Validacion de mayor a 13 caracteres -->
			<?php if (isset($_SESSION['val3'])) { ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<?= $_SESSION['val3']?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php session_unset(); } ?>

			<!-- Validacion de solo numeros caracteres -->
			<?php if (isset($_SESSION['val4'])) { ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<?= $_SESSION['val4']?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php session_unset(); } ?>

			<!-- Validacion de solo numeros caracteres -->
			<?php if (isset($_SESSION['val5'])) { ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<?= $_SESSION['val5']?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php session_unset(); } ?>

		</div>
	</div>

	<div class="row justify-content-center">
		<div class="col">
			<form method="POST" action="controller/showinfo.php" enctype="multipart/form-data">
				<div class="form-group">
					<label for="space-cui" class="pl-1">Digite su numero de CUI:</label>
					<input type="text" name="cui" class="form-control" id="space-cui">
				</div>
				<div class="text-center">
					<button type="submit" name="Submit" class="btn btn-success">Buscar</button>
				</div>

				<!-- <?php
						require_once('library/recaptcha/recaptchalib.php');
						$publickey = "6Lc3Wn0fAAAAAApxUTMuDrrO-n273zmmbCYXsSpq"; // you got this from the signup page
						echo recaptcha_get_html($publickey);
						?> -->
			</form>
		</div>
	</div>
</div>
<?php include("resources/views/footer.php") ?>
